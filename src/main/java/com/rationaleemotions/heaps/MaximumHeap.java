package com.rationaleemotions.heaps;

import java.util.function.Function;

public class MaximumHeap<T extends Comparable<T>> extends Heap<T> {

    public MaximumHeap(Class<T> clazz) {
        super(clazz);
    }

    protected void bubbleUpHighestPriorityElement() {
        int index = position;
        int parentIndex = index / 2;
        while (parentIndex >= 1 && heap[index].compareTo(heap[parentIndex]) > 0) {
            swap(index, parentIndex);
            index = parentIndex;
            parentIndex = index / 2;
        }
    }


    protected void pushRootDownToBeHeapCompliant(int endIndex) {
        if (endIndex == 0) {
            return;
        }
        int index = 1;
        while (index <= endIndex) {
            int leftChildIndex = LEFT_CHILD_INDEX.apply(index);
            int rightChildIndex = RIGHT_CHILD_INDEX.apply(index);
            if (leftChildIndex > endIndex) {
                break;
            }

            int childToSwap = rightChildIndex > endIndex
                    ? leftChildIndex
                    : heap[leftChildIndex].compareTo(heap[rightChildIndex]) > 0
                    ? leftChildIndex
                    : rightChildIndex;

            if (heap[index].compareTo(heap[childToSwap]) > 0) {
                break;
            }
            swap(index, childToSwap);
            index = childToSwap;
        }
    }

}