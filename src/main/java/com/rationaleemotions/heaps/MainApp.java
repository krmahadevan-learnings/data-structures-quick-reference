package com.rationaleemotions.heaps;

import java.util.Arrays;
import java.util.List;

public class MainApp {

    /*
     * Video Reference: https://youtu.be/L3hfc7Hvirw
     */
    public static void main(String[] args) {
        List<Integer> data = Arrays.asList(50, 40, 60, 30, 70, 20, 100);
        minHeapDemo(data);
        maxHeapDemo(data);
    }

    private static void minHeapDemo(List<Integer> data) {
        System.err.println("Minimum Heap Demo\n");
        Heap<Integer> heap = new MinimumHeap<>(Integer.class);
        data.forEach(heap::insert);
        Integer current;
        do {
            current = heap.nextPrioritisedElement();
            if (current != null) {
                System.err.println("Next minimum element in heap = " + current);
            }
        } while (current != null);

    }

    private static void maxHeapDemo(List<Integer> data) {
        System.err.println("Maximum Heap Demo\n");
        Heap<Integer> heap = new MaximumHeap<>(Integer.class);
        data.forEach(heap::insert);
        Integer current;
        do {
            current = heap.nextPrioritisedElement();
            if (current != null) {
                System.err.println("Next maximum element in heap = " + current);
            }
        } while (current != null);
    }

}
