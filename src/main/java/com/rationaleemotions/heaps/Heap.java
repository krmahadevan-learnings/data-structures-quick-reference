package com.rationaleemotions.heaps;

import java.lang.reflect.Array;
import java.util.function.Function;

public abstract class Heap<T extends Comparable<T>> {

    private static final int DEFAULT_SIZE = 16;
    protected static final Function<Integer, Integer> LEFT_CHILD_INDEX = i -> 2 * i;
    protected static final Function<Integer, Integer> RIGHT_CHILD_INDEX = i -> (2 * i) + 1;

    protected T[] heap;
    protected int position = 0;

    @SuppressWarnings("unchecked")
    public Heap(Class<T> clazz) {
        heap = (T[]) Array.newInstance(clazz, DEFAULT_SIZE);
    }

    protected abstract void bubbleUpHighestPriorityElement();

    protected abstract void pushRootDownToBeHeapCompliant(int endIndex);

    public void insert(T data) {
        if (isFull()) {
            resize(2 * heap.length);
        }
        heap[++position] = data;
        bubbleUpHighestPriorityElement();
    }

    public T nextPrioritisedElement() {
        if (isEmpty()) {
            return null;
        }
        T result = heap[1];
        heap[1] = heap[position];
        heap[position] = null;
        position -= 1;
        pushRootDownToBeHeapCompliant(position);
        return result;
    }

    protected void swap(int firstIndex, int secondIndex) {
        T temp = heap[firstIndex];
        heap[firstIndex] = heap[secondIndex];
        heap[secondIndex] = temp;
    }

    @SuppressWarnings("unchecked")
    private void resize(int capacity) {
        System.arraycopy(heap, 0, heap = (T[]) new Comparable[capacity], 0, position + 1);
    }

    private boolean isFull() {
        return position == heap.length - 1;
    }

    private boolean isEmpty() {
        return heap.length == 0;
    }

}
