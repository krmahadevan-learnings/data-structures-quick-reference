package com.rationaleemotions.cache;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LRUCache {
    private final int capacity;
    private final Map<String, Object> data;
    private final AtomicInteger current = new AtomicInteger(0);
    private final Deque<String> usageTracker = new ConcurrentLinkedDeque<>();

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public LRUCache(int capacity) {
        this.capacity = capacity;
        data = new ConcurrentHashMap<>(capacity);
    }

    public void add(String key, Object value) {
        lock.writeLock().lock();
        try {
            if (current.incrementAndGet() > capacity) {
                evict();
            }
            data.put(key, value);
            usageTracker.addFirst(key);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Object get(String key) {
        if (key == null) {
            throw new IllegalArgumentException("key cannot be null");
        }

        Object result = data.get(key);
        if (result != null && !Optional.ofNullable(usageTracker.peekFirst()).orElse("").equals(key)) {
            //Don't move elements if this same key is being repeatedly read from the cache, because we are already
            // in the front of the queue indicating that we were recently used.
            lock.writeLock().lock();
            try {
                usageTracker.remove(key);
                usageTracker.addFirst(key);
            } finally {
                lock.writeLock().unlock();
            }
        }
        return result;
    }

    private void evict() {
        data.remove(usageTracker.removeLast());
    }
}
