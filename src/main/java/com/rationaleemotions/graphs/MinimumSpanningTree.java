package com.rationaleemotions.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimumSpanningTree<T> {

    private final List<Element<T>> elements = new ArrayList<>();

    public void addElement(Vertex<T> from, Neighbour<T> to) {
        elements.add(new Element<>(from, to.vertex(), to.edge()));
    }

    public List<Element<T>> elements() {
        return Collections.unmodifiableList(elements);
    }

    public int totalWeight() {
        return elements.stream()
                .map(it -> it.edge)
                .mapToInt(Edge::getWeight)
                .sum();
    }

    public record Element<T> (Vertex<T> from, Vertex<T> to, Edge edge) {

        @Override
        public String toString() {
            return from.getData() + "--> (" + edge.getWeight() + ") -->" + to.getData();
        }
    }
}
