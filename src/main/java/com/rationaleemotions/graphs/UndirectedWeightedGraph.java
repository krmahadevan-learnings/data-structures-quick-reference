package com.rationaleemotions.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UndirectedWeightedGraph<T> implements IGraph<T> {

    private final List<Vertex<T>> vertices = new ArrayList<>();

    public void addEdge(T from, T to, int weight) {
        Vertex<T> source = new Vertex<>(from);
        vertices.add(source);
        Vertex<T> destination = new Vertex<>(to);
        vertices.add(destination);
        Edge edge = new Edge(weight);
        source.addNeighbour(destination, edge);
        destination.addNeighbour(source, edge);
    }

    public List<Vertex<T>> getVertices() {
        return Collections.unmodifiableList(vertices);
    }
}
