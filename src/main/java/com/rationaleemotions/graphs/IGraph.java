package com.rationaleemotions.graphs;

import java.util.List;

public interface IGraph<T> {

    List<Vertex<T>> getVertices();
}
