package com.rationaleemotions.graphs;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class FordFulkerson<T> {

    private final List<List<Vertex<T>>> paths = new ArrayList<>();

    public int run(Vertex<T> source, Vertex<T> destination) {
        findAllPaths(source, destination, new ArrayList<>(Collections.singleton(source)));
        AtomicInteger maxFlow = new AtomicInteger();
        paths.stream()
                .sorted(Comparator.comparingInt(this::getMinFlowInPath).reversed())
                .forEach(path -> {
                    Integer minimum = getMinFlowInPath(path);
                    prettyPrintPathAndMinFlow(path, minimum);
                    removeMinFlowFromVerticesInPath(path, minimum);
                    maxFlow.addAndGet(minimum);
                });
        return maxFlow.get();
    }

    private static <T> void removeMinFlowFromVerticesInPath(List<Vertex<T>> path, int minimum) {
        Vertex<T> previous = path.get(0);
        for (int i = 1; i < path.size(); i++) {
            Vertex<T> current = path.get(i);
            findNeighbourMatchingNextVertex(previous, current)
                    .ifPresent(found -> found.edge().reduceWeightBy(minimum));
            previous = current;
        }
    }

    private static <T> void prettyPrintPathAndMinFlow(List<Vertex<T>> path, int minimum) {
        String text = path.stream()
                .map(Vertex::getData)
                .map(Object::toString)
                .collect(Collectors.joining("->")) + " Min(" + minimum + ")";
        System.err.println(text);
    }

    private Integer getMinFlowInPath(List<Vertex<T>> path) {
        Vertex<T> previous = path.get(0);
        AtomicReference<Integer> min = new AtomicReference<>(Integer.MAX_VALUE);
        for (int i = 1; i < path.size(); i++) {
            Vertex<T> current = path.get(i);
            findNeighbourMatchingNextVertex(previous, current)
                    .ifPresent(found -> updateMinimumTo(found, min));
            previous = current;
        }
        return min.get();
    }

    private static <T> void updateMinimumTo(Neighbour<T> found, AtomicReference<Integer> currentMinimum) {
        if (currentMinimum.get() > found.edge().getWeight()) {
            currentMinimum.set(found.edge().getWeight());
        }
    }

    private static <T> Optional<Neighbour<T>> findNeighbourMatchingNextVertex(Vertex<T> current, Vertex<T> next) {
        return current.getNeighbours()
                .stream()
                .filter(it -> it.vertex().equals(next))
                .findFirst();
    }

    private void findAllPaths(Vertex<T> current, Vertex<T> destination, List<Vertex<T>> currentPath) {
        if (current.equals(destination)) {
            paths.add(new ArrayList<>(currentPath));
            return;
        }
        current.setVisited(true);
        current.getNeighbours().stream()
                .filter(neighbor -> neighbor.vertex().isNotVisited())
                .forEach(neighbor -> {
                    currentPath.add(neighbor.vertex());
                    findAllPaths(neighbor.vertex(), destination, currentPath);
                    currentPath.remove(neighbor.vertex());
                });
        current.setVisited(false);
    }

}
