package com.rationaleemotions.graphs.traverse;

import com.rationaleemotions.graphs.IGraph;
import com.rationaleemotions.graphs.Neighbour;
import com.rationaleemotions.graphs.Vertex;

import java.util.*;

public enum GraphWalker implements Traverse {

    DepthFirst {
        @Override
        public <T> List<Vertex<T>> traverse(IGraph<T> graph) {
            List<Vertex<T>> result = new ArrayList<>();
            Stack<Vertex<T>> stack = new Stack<>();
            Vertex<T> start = graph.getVertices().get(0);
            stack.push(start);
            while (!stack.isEmpty()) {
                Vertex<T> current = stack.pop();
                if (current.isVisited()) {
                    continue;
                }
                current.setVisited(true);
                result.add(current);
                Collections.reverse(current.getNeighbours()); // Preserve order of the neighbours inserted
                for (Neighbour<T> neighbour : current.getNeighbours()) {
                    stack.push(neighbour.vertex());
                }
            }
            return result;
        }
    },
    DepthFirstRecursive {
        @Override
        public <T> List<Vertex<T>> traverse(IGraph<T> graph) {
            List<Vertex<T>> result = new ArrayList<>();
            traverse(graph.getVertices().get(0), result);
            return result;
        }

        public <T> void traverse(Vertex<T> start, List<Vertex<T>> result) {
            start.setVisited(true);
            result.add(start);
            start.getNeighbours().stream()
                    .filter(Neighbour::canConsider)
                    .map(Neighbour::vertex)
                    .forEach(it -> traverse(it, result));
        }
    },
    BreadFirst {
        @Override
        public <T> List<Vertex<T>> traverse(IGraph<T> graph) {
            List<Vertex<T>> result = new ArrayList<>();
            Queue<Vertex<T>> queue = new LinkedList<>();
            Vertex<T> start = graph.getVertices().get(0);
            queue.add(start);
            while (!queue.isEmpty()) {
                Vertex<T> current = queue.poll();
                if (current.isVisited()) {
                    continue;
                }
                current.setVisited(true);
                result.add(current);
                current.getNeighbours().forEach(it -> queue.add(it.vertex()));
            }
            return result;
        }
    },
    DijkshtraShortestPath {
        @Override
        public <T> List<Vertex<T>> traverse(IGraph<T> graph) {
            Vertex<T> start = graph.getVertices().get(0);
            List<Vertex<T>> shortestPath = new ArrayList<>();
            calculateShortestPath(start, shortestPath);
            return shortestPath;
        }

        private static <T> void calculateShortestPath(Vertex<T> source, List<Vertex<T>> shortestPath) {
            source.setDistance(0);
            Set<Vertex<T>> settledNodes = new HashSet<>();
            Queue<Vertex<T>> unsettledNodes = new PriorityQueue<>(Collections.singleton(source));
            while (!unsettledNodes.isEmpty()) {
                Vertex<T> currentNode = unsettledNodes.poll();
                System.err.println("Current Node " + currentNode);
                currentNode.getNeighbours().stream()
                        .filter(neighbour -> !settledNodes.contains(neighbour.vertex()))
                        .forEach(neighbour -> {
                            evaluateDistanceAndPath(currentNode, neighbour, shortestPath);
                            unsettledNodes.add(neighbour.vertex());
                        });
                if (currentNode.getNeighbours().isEmpty()) {
                    //We perhaps hit the end of the Directed Graph.
                    if (!shortestPath.contains(currentNode)) {
                        shortestPath.add(currentNode);
                    }
                }
                settledNodes.add(currentNode);
            }
        }

        private static <T> void evaluateDistanceAndPath(Vertex<T> sourceNode, Neighbour<T> neighbour, List<Vertex<T>> shortestPath) {
            Integer newDistance = sourceNode.getDistance() + neighbour.edge().getWeight();
            if (newDistance <= neighbour.vertex().getDistance()) {
                neighbour.vertex().setDistance(newDistance);
                if (!shortestPath.contains(sourceNode)) {
                    shortestPath.add(sourceNode);
                }
            }
        }

    }
}
