package com.rationaleemotions.graphs.traverse;

import com.rationaleemotions.graphs.IGraph;
import com.rationaleemotions.graphs.Vertex;

import java.util.List;

public interface Traverse {

    <T> List<Vertex<T>> traverse(IGraph<T> graph);

}
