package com.rationaleemotions.graphs;

import com.rationaleemotions.graphs.traverse.GraphWalker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MainApp {

    public static void main(String[] args) {
//        demoCycleDetectionUsingTarjanAlgorithm();
        demoTopologicalSort();
//        demoFordFulkerson();
//        demoDijkstra();
//        demoDFS();
//        demoDFSUsingRecursion();
//        demoBFS();
//        demoMstUsingPrimAlgorithm();
    }

    private static void demoCycleDetectionUsingTarjanAlgorithm() {
        System.err.println("Printing the cycle");
        DirectedWeightedGraph<String> graph = generateCyclicGraph();
        String text = new Tarjan<>(graph, graph.getVertexWithData("1")).detectCycle()
                .stream()
                .map(Vertex::getData)
                .collect(Collectors.joining("->"));
        System.err.println(text);
    }

    private static void demoTopologicalSort() {
        System.err.println("Printing the topological order for  DAG");
        String text = TopologicalSort.sort(generateDAG()).stream()
                .map(Vertex::getData)
                .collect(Collectors.joining("->"));
        System.err.println(text);

        System.err.println("Printing the topological order for a cyclic graph");
        text = TopologicalSort.sort(generateCyclicGraph()).stream()
                .map(Vertex::getData)
                .collect(Collectors.joining("->"));
        System.err.println(text);
    }

    private static DirectedWeightedGraph<String> generateCyclicGraph() {
        DirectedWeightedGraph<String> graph = new DirectedWeightedGraph<>();
        Vertex<String> zero = Vertex.newInstance("0");
        graph.addVertex(zero);
        Vertex<String> one = Vertex.newInstance("1");
        graph.addVertex(one);
        Vertex<String> two = Vertex.newInstance("2");
        graph.addVertex(two);
        Vertex<String> three = Vertex.newInstance("3");
        graph.addVertex(three);
        Vertex<String> four = Vertex.newInstance("4");
        graph.addVertex(four);
        Vertex<String> five = Vertex.newInstance("5");
        graph.addVertex(five);

        zero.addNeighbour(one, three);
        two.addNeighbour(zero, four);
        three.addNeighbour(one);
        four.addNeighbour(three, five);
        five.addNeighbour(one);
        one.addNeighbour(five); //cycle
        return graph;
    }

    private static DirectedWeightedGraph<String> generateDAG() {
        DirectedWeightedGraph<String> graph = new DirectedWeightedGraph<>();
        Vertex<String> zero = Vertex.newInstance("0");
        graph.addVertex(zero);
        Vertex<String> one = Vertex.newInstance("1");
        graph.addVertex(one);
        Vertex<String> two = Vertex.newInstance("2");
        graph.addVertex(two);
        Vertex<String> three = Vertex.newInstance("3");
        graph.addVertex(three);
        Vertex<String> four = Vertex.newInstance("4");
        graph.addVertex(four);
        Vertex<String> five = Vertex.newInstance("5");
        graph.addVertex(five);

        zero.addNeighbour(one, three);
        two.addNeighbour(zero, four);
        three.addNeighbour(one);
        four.addNeighbour(three, five);
        five.addNeighbour(one);
        return graph;
    }

    private static void demoFordFulkerson() {
        Vertex<String> S = new Vertex<>("S");
        Vertex<String> A = new Vertex<>("A");
        Vertex<String> B = new Vertex<>("B");
        Vertex<String> C = new Vertex<>("C");
        Vertex<String> D = new Vertex<>("D");
        Vertex<String> T = new Vertex<>("T");

        S.addNeighbour(A, 7);
        S.addNeighbour(D, 4);

        A.addNeighbour(B, 5);
        A.addNeighbour(C, 3);

        B.addNeighbour(T, 8);

        C.addNeighbour(B, 3);
        C.addNeighbour(T, 5);

        D.addNeighbour(A, 3);
        D.addNeighbour(C, 2);
        System.out.print("The Max Flow is: ");
        System.out.println(new FordFulkerson<String>().run(S, T));
    }

    private static void demoFordFulkersond() {
        Vertex<String> S = new Vertex<>("S");
        Vertex<String> A = new Vertex<>("A");
        Vertex<String> B = new Vertex<>("B");
        Vertex<String> C = new Vertex<>("C");
        Vertex<String> D = new Vertex<>("D");
        Vertex<String> T = new Vertex<>("T");

//        S.setNeighbors(new HashMap<>(Map.of(A, 15, B, 12)));
        S.addNeighbour(A, 15);
        S.addNeighbour(B, 12);

//        A.setNeighbors(new HashMap<>(Map.of(B, 10, C, 12, D, 1)));
        A.addNeighbour(B, 10);
        A.addNeighbour(C, 12);
        A.addNeighbour(D, 1);

//        B.setNeighbors(new HashMap<>(Map.of(D, 14)));
        B.addNeighbour(D, 14);
//        C.setNeighbors(new HashMap<>(Map.of(T, 25)));
        C.addNeighbour(T, 25);

//        D.setNeighbors(new HashMap<>(Map.of(C, 10, T, 4)));
        D.addNeighbour(C, 10);
        D.addNeighbour(T, 4);

        System.out.print("The Max Flow is: ");
        System.out.println(new FordFulkerson<String>().run(S, T));
    }

    private static void demoDijkstra() {
        IGraph<String> graph = newDirectedWeightedGraph();
        List<Vertex<String>> result = GraphWalker.DijkshtraShortestPath.traverse(graph);
        System.err.println("Printing the shortest path");
        result.forEach(it -> System.err.println(it.toString()));
    }

    private static void demoDFSUsingRecursion() {
        IGraph<String> graph = newUndirectedGraph();
        List<Vertex<String>> result = GraphWalker.DepthFirst.traverse(graph);
        System.err.println("Printing the Depth First Traversal (Recursive) for the Graph");
        result.forEach(it -> System.err.println(it.toString()));
    }

    private static void demoDFS() {
        IGraph<String> graph = newUndirectedGraph();
        List<Vertex<String>> result = GraphWalker.DepthFirst.traverse(graph);
        System.err.println("Printing the Depth First Traversal (Iterative) for the Graph");
        result.forEach(it -> System.err.println(it.toString()));
    }

    private static void demoBFS() {
        IGraph<String> graph = newUndirectedGraph();
        List<Vertex<String>> result = GraphWalker.BreadFirst.traverse(graph);
        System.err.println("Printing the Breadth First Traversal for the Graph");
        result.forEach(it -> System.err.println(it.toString()));
    }

    private static void demoMstUsingPrimAlgorithm() {
        IGraph<String> graph = newUndirectedWeightedGraph();
        MinimumSpanningTree<String> result = MSTAlgorithm.Prim.buildMinimumSpanningTree(graph);
        System.err.println("Printing the Minimum Spanning Tree (MST) using Prim's algorithm");
        result.elements().forEach(it -> System.err.println(it.toString()));
        System.err.println("Total Weight of the generated MST: " + result.totalWeight());
    }

    private static IGraph<String> newDirectedWeightedGraph() {
        DirectedWeightedGraph<String> graph = new DirectedWeightedGraph<>();
        Vertex<String> a = new Vertex<>("A");
        Vertex<String> b = new Vertex<>("B");
        Vertex<String> c = new Vertex<>("C");
        Vertex<String> d = new Vertex<>("D");
        Vertex<String> e = new Vertex<>("E");
        Vertex<String> f = new Vertex<>("F");

        a.addNeighbour(b, 2);
        a.addNeighbour(c, 4);
        graph.addVertex(a);

        b.addNeighbour(c, 3);
        b.addNeighbour(d, 1);
        b.addNeighbour(e, 5);
        graph.addVertex(b);

        c.addNeighbour(d, 2);
        graph.addVertex(c);

        d.addNeighbour(e, 1);
        d.addNeighbour(f, 4);
        graph.addVertex(d);

        e.addNeighbour(f, 2);
        graph.addVertex(e);

        graph.addVertex(f);
        return graph;
    }

    private static IGraph<String> newUndirectedGraph() {
        UndirectedGraph<String> graph = new UndirectedGraph<>();
        Vertex<String> a = new Vertex<>("a");
        Vertex<String> b = new Vertex<>("b");
        Vertex<String> c = new Vertex<>("c");
        Vertex<String> d = new Vertex<>("d");
        Vertex<String> e = new Vertex<>("e");
        Vertex<String> f = new Vertex<>("f");
        a.addNeighbour(b, f);
        graph.addVertex(a);
        b.addNeighbour(a, c, f);
        graph.addVertex(b);
        c.addNeighbour(b, d, f);
        graph.addVertex(c);
        d.addNeighbour(c, e);
        graph.addVertex(d);
        e.addNeighbour(d, e);
        graph.addVertex(e);
        f.addNeighbour(a, b, c, e);
        graph.addVertex(f);
        return graph;
    }

    private static IGraph<String> newUndirectedWeightedGraph() {
        UndirectedWeightedGraph<String> graph = new UndirectedWeightedGraph<>();
        graph.addEdge("a", "b", 4);
        graph.addEdge("a", "f", 2);

        graph.addEdge("b", "f", 2);
        graph.addEdge("b", "c", 6);

        graph.addEdge("c", "f", 3);
        graph.addEdge("c", "d", 3);

        graph.addEdge("d", "e", 2);

        graph.addEdge("e", "f", 4);
        return graph;
    }
}
