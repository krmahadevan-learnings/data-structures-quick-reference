package com.rationaleemotions.graphs;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Comparator.comparingInt;

public enum MSTAlgorithm implements IMSTGenerator {

    // Refer to video at https://www.youtube.com/watch?v=gUHFHy0WXcM
    Prim {
        @Override
        public <T> MinimumSpanningTree<T> buildMinimumSpanningTree(IGraph<T> graph) {
            MinimumSpanningTree<T> result = new MinimumSpanningTree<>();
            List<Vertex<T>> vertices = graph.getVertices();
            if (!vertices.isEmpty()) {
                vertices.get(0).setVisited(true);
            }
            while (vertices.stream().anyMatch(Vertex::isNotVisited)) {
                AtomicReference<Vertex<T>> current = new AtomicReference<>();
                vertices.stream()
                        .filter(Vertex::isVisited) //Only consider visited vertices
                        .peek(current::set) // Note down the vertex being processed. It will be our "From" value
                        .flatMap(each -> each.getNeighbours().stream()) // Get all the current vertex neighbours
                        .filter(Neighbour::canConsider)// Look at ONLY those neighbours that can be considered
                        //A neighbour can be considered ONLY if
                        // a. it's vertex NOT yet visited and
                        // b. the edge to it is NOT already added.
                        .min(comparingInt(n -> n.edge().getWeight()))// We need the edge with minimum weight
                        .ifPresent(candidate -> {
                            //We found such a neighbour. So mark it as the vertex visited and
                            // the edge that we considered to be included,
                            // so that the same edge from "To" to "From" is NOT added back.
                            candidate.vertex().setVisited(true);
                            candidate.edge().setIncluded(true);
                            System.err.println(candidate.logMessage());
                            // Lets add the current vertex and the found neighbour to our MST
                            result.addElement(current.get(), candidate);
                        });
            }
            return result;
        }
    }

}
