package com.rationaleemotions.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * This class does topological sort on a directed graph (assumes that there are no cycles).
 * We use Kahn's algorithm here.
 * <p>
 * 1. Find independent nodes (no incoming edges)
 * 2. Remove independent nodes and add them to a queue.
 * 3. For each element in queue, add independent node to a result list.
 * 4. Repeat from (1) until queue is empty.
 * 5. If either there are no independent nodes at any given point in time, then we detected a cycle.
 *
 */
public class TopologicalSort {

    public static <T> List<Vertex<T>> sort(IGraph<T> graph) {
        List<Vertex<T>> result = new ArrayList<>();
        Queue<Vertex<T>> queue = new LinkedList<>();
        graph.getVertices()
                .stream()
                .filter(it -> it.getPredecessors().isEmpty())
                .forEach(queue::add);
        if (queue.isEmpty()) {
            throw new IllegalStateException("Graph has no independent nodes to start with.");
        }
        int count = 0;
        while (!queue.isEmpty()) {
            Vertex<T> popped = queue.poll();
            ++count;
            result.add(popped);
            List<Vertex<T>> next = popped.getNeighbours()
                    .stream()
                    .map(Neighbour::vertex)
                    .filter(it -> {
                        it.getPredecessors().remove(popped);
                        return it.getPredecessors().isEmpty();
                    })
                    .toList();
            boolean isCycleDetected = next.isEmpty() // There are no more independent nodes
                    && queue.isEmpty() // Nothing in queue which means we processed all vertices
                    && count != graph.getVertices().size(); // But our processed vertex count is NOT matching graph's vertex count
            if (isCycleDetected) {
                String text = new Tarjan<>(graph, popped.getNeighbours().get(0).vertex()).detectCycle()
                        .stream()
                        .map(it -> it.getData().toString())
                        .collect(Collectors.joining("->"));
                throw new IllegalStateException("Detected a cycle: " + text);
            }
            queue.addAll(next);
        }
        return result;
    }
}
