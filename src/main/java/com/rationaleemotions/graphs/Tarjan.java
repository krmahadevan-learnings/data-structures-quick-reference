package com.rationaleemotions.graphs;

import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
public class Tarjan<T> {

    private final IGraph<T> graph;
    private final Vertex<T> start;
    private final Map<Vertex<T>, Integer> visitedVertices = new HashMap<>();
    private final Map<Vertex<T>, Integer> lowLinks = new HashMap<>();
    private final Stack<Vertex<T>> stack = new Stack<>();
    private final AtomicInteger id = new AtomicInteger(1);
    private final List<Vertex<T>> cycle = new ArrayList<>();


    public List<Vertex<T>> detectCycle() {
        detectCycle(start);
        return cycle;
    }

    private void detectCycle(Vertex<T> start) {
        int idToUse = id.getAndIncrement();
        visitedVertices.put(start, idToUse);
        lowLinks.put(start, idToUse);
        stack.push(start);
        List<Vertex<T>> neighbours = start.getNeighbours().stream().map(Neighbour::vertex).toList();
        for (Vertex<T> neighbour : neighbours) {
            if (!visitedVertices.containsKey(neighbour)) {
                detectCycle(neighbour);
                int min = Math.min(lowLinks.get(start), lowLinks.get(neighbour));
                lowLinks.put(start, min);
            } else if (stack.contains(neighbour)) {
                int min = Math.min(lowLinks.get(start), visitedVertices.get(neighbour));
                lowLinks.put(start, min);
            }
        }

        if (Objects.equals(visitedVertices.get(start), lowLinks.get(start))) {
            //So we finally found a cycle
            cycle.clear();
            Vertex<T> popped;
            do {
                popped = stack.pop();
                cycle.add(popped);
            } while (!popped.equals(start));
        }

    }
}
