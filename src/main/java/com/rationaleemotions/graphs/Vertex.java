package com.rationaleemotions.graphs;

import lombok.*;

import java.util.*;

@Data
public class Vertex<T> implements Comparable<Vertex<T>> {

    private final T data;
    private boolean isVisited;
    private Integer distance = Integer.MAX_VALUE;

    @ToString.Exclude
    private List<Neighbour<T>> neighbours = new LinkedList<>();

    @ToString.Exclude
    private List<Vertex<T>> predecessors = new LinkedList<>();

    public static <T> Vertex<T> newInstance(T data) {
        return new Vertex<>(data);
    }

    public boolean isNotVisited() {
        return !isVisited;
    }

    @SafeVarargs
    public final void addNeighbour(Vertex<T>... vertices) {
        Arrays.stream(vertices).forEach(it -> {
            neighbours.add(new Neighbour<>(it));
            it.predecessors.add(this);
        });
    }

    public void addNeighbour(Vertex<T> vertex, int weight) {
        addNeighbour(vertex, new Edge(weight));
    }

    public void addNeighbour(Vertex<T> vertex, Edge edge) {
        neighbours.add(new Neighbour<>(vertex, edge));
        vertex.predecessors.add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex<?> vertex = (Vertex<?>) o;
        return Objects.equals(data, vertex.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public int compareTo(Vertex<T> o) {
        return Integer.compare(distance, o.distance);
    }
}
