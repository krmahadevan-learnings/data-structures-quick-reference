package com.rationaleemotions.graphs;

interface IMSTGenerator {
    <T> MinimumSpanningTree<T> buildMinimumSpanningTree(IGraph<T> graph);
}
