package com.rationaleemotions.graphs;

import java.util.ArrayList;
import java.util.List;

public class DirectedWeightedGraph<T> implements IGraph<T> {
    private final List<Vertex<T>> vertices = new ArrayList<>();

    public void addVertex(Vertex<T> vertex) {
        vertices.add(vertex);
    }
    @Override
    public List<Vertex<T>> getVertices() {
        return vertices;
    }

    public Vertex<T> getVertexWithData(T data) {
        return vertices.stream()
                .filter(it -> it.getData().equals(data))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(data + " is not a valid vertex on the graph"));
    }
}
