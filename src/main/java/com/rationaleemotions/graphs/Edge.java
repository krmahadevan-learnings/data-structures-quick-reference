package com.rationaleemotions.graphs;

import lombok.*;

@Data
public class Edge {

    public static final Edge DUMMY = new Edge(-1);

    private int weight;
    private boolean isIncluded;

    public Edge(int weight) {
        this.weight = weight;
    }

    public void reduceWeightBy(int reduceBy) {
        if ((weight-reduceBy) >= 0) {
            weight = weight - reduceBy;
        }
    }
}
