package com.rationaleemotions.graphs;


public record Neighbour<T>(Vertex<T> vertex, Edge edge) {

    public Neighbour(Vertex<T> vertex) {
        this(vertex, Edge.DUMMY);
    }

    /**
     * @return <code>true</code> - if the vertex has NOT been visited YET and if the edge has NOT been included
     * by any other traversal.
     */
    public boolean canConsider() {
        if (ignoreEdge()) {
            return !vertex().isVisited();
        }
        return !(vertex.isVisited() || edge.isIncluded());
    }

    public String logMessage() {
        if (ignoreEdge()) {
            return "Reached " + vertex;
        }
        return "Reached " + vertex + " via edge with weight " + edge.getWeight();
    }

    private boolean ignoreEdge() {
        return edge == Edge.DUMMY;
    }
}
