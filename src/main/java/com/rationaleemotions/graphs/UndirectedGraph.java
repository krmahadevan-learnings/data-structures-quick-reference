package com.rationaleemotions.graphs;

import java.util.ArrayList;
import java.util.List;

public class UndirectedGraph<T> implements IGraph<T> {

    private final List<Vertex<T>> vertices = new ArrayList<>();

    public void addVertex(Vertex<T> vertex) {
        vertices.add(vertex);
    }

    @Override
    public List<Vertex<T>> getVertices() {
        return vertices;
    }
}
