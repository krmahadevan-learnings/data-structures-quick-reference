package com.rationaleemotions.trees.bst;

import com.rationaleemotions.trees.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public enum IterativeTraverseOrder implements Walk {

    POST_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            Stack<Node<T>> stack = new Stack<>();
            Node<T> pointer = start;
            while (pointer != null || !stack.isEmpty()) {
                if (pointer != null) {
                    stack.push(pointer);
                    pointer = pointer.getLeftChild();
                    continue;
                }
                //If we are here, then it means that we processed left side
                Node<T> temp = stack.peek().getRightChild();
                if (temp == null) {
                    //We hit a leaf node.
                    temp = stack.pop();
                    contents.add(temp.getData());
                    while (!stack.isEmpty() && temp == stack.peek().getRightChild()) {
                        //As long as the leaf node is the right child of the top element on the stack
                        // keep popping and keep processing that node.
                        temp = stack.pop();
                        contents.add(temp.getData());
                    }
                    continue;
                }
                //If the top of the stack has a right side, then we need to add
                //them to the stack as well.
                pointer = temp;
            }
            return contents;
        }
    },

    PRE_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            Stack<Node<T>> stack = new Stack<>();
            Node<T> pointer = start;
            while (pointer != null || !stack.isEmpty()) {
                if (pointer != null) {
                    contents.add(pointer.getData());
                    stack.push(pointer);
                    pointer = pointer.getLeftChild();
                    continue; // keep going left;
                }
                //If we are here, it means that we have reached the left end of the tree
                pointer = stack.pop().getRightChild();
            }

            return contents;
        }
    },
    IN_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            Stack<Node<T>> stack = new Stack<>();
            Node<T> pointer = start;
            while (pointer != null || !stack.isEmpty()) {
                if (pointer != null) {
                    stack.push(pointer);
                    pointer = pointer.getLeftChild();
                    continue; // keep going left
                }
                //If we are here, it means that we have reached the left end of the tree
                pointer = stack.pop();
                contents.add(pointer.getData());
                pointer = pointer.getRightChild();
            }
            return contents;
        }
    }
}
