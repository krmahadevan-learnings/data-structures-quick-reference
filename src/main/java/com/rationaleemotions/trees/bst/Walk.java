package com.rationaleemotions.trees.bst;

import com.rationaleemotions.trees.Node;

import java.util.List;

interface Walk {
    <T extends Comparable<T>> List<T> tread(Node<T> start);
}
