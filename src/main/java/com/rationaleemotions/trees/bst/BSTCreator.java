package com.rationaleemotions.trees.bst;

import com.rationaleemotions.trees.Node;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class BSTCreator {

    public static void main(String[] args) {

        MyBstTree<Integer> tree = new MyBstTree<>();
//        Arrays.asList(2,3,4,5,6,7)
//                .forEach(tree::insert);
//        System.err.println(tree.traverse());
//        tree.delete(5);
//        System.err.println(tree.traverse());

        tree = new MyBstTree<>();
        tree.generate(Arrays.asList(30, 20, 10, 15, 25, 40, 50, 45));
        System.err.println(tree.traverse());

    }

    public static class MyBstTree<T extends Comparable<T>> {
        private Node<T> root;

        void generate(List<T> preOrder) {
            if (preOrder.isEmpty()) {
                return;
            }

            root = new Node<>(preOrder.get(0));
            Stack<Node<T>> stack = new Stack<>();
            Node<T> start = root;
            for (int i = 1; i < preOrder.size(); ) {
                T each = preOrder.get(i);
                int result = each.compareTo(start.getData());
                if (result < 0) {
                    //We found a left child
                    stack.push(start);
                    start.setLeftChild(new Node<>(each));
                    start = start.getLeftChild();
                    i++;
                    continue;
                }
                if (result == 0) {
                    //This should have never happened. because we cannot have duplicates in a BST
                    i++;
                    continue;
                }
                //If we are here, then it means we are dealing with a right child
                if (stack.isEmpty()) {
                    //There are no elements in the stack. So just add this value as right child and move on.
                    start.setRightChild(new Node<>(each));
                    start = start.getRightChild();
                    i++;
                } else {
                    //Ok so we have some values in the stack, which means we need to backtrack to the right
                    //parent before adding this value as a right child.
                    T valueAtTopOfStack = stack.peek().getData();
                    int comparisonResult = each.compareTo(valueAtTopOfStack);
                    if (comparisonResult < 0) {
                        //Ok, so if found a parent. Lets add the value as a right child.
                        start.setRightChild(new Node<>(each));
                        start = start.getRightChild();
                        i++;
                    }
                    if (comparisonResult > 0) {
                        //We didn't find the parent. Continue popping elements in the stack until we find ourselves
                        //a suitable parent.
                        start = stack.pop();
                    }
                }
            }

        }

        void insert(T data) {
            Node<T> node = insert(root, data);
            if (root == null) {
                root = node;
            }
        }

        void delete(T data) {
            Node<T> deleted = delete(root, data);
            if (deleted == null) {
                throw new IllegalArgumentException(data + " not found");
            }
        }

        private static <T extends Comparable<T>> Node<T> delete(Node<T> root, T data) {
            if (root == null) {
                return root;
            }
            int result = data.compareTo(root.getData());
            if (result > 0) {
                root.setRightChild(delete(root.getRightChild(), data));
            } else if (result < 0) {
                root.setLeftChild(delete(root.getLeftChild(), data));
            } else {
                //We found our node
                if (root.isLeafNode()) {
                    root = null;
                } else if (root.getLeftChild() != null) {
                    Node<T> predecessorNode = predecessor(root.getLeftChild());
                    root.setData(predecessorNode.getData());
                    root.setLeftChild(delete(root.getLeftChild(), predecessorNode.getData()));
                } else {
                    Node<T> successorNode = successor(root.getRightChild());
                    root.setData(successorNode.getData());
                    root.setRightChild(delete(root.getRightChild(), successorNode.getData()));
                }
            }
            return root;

        }

        private static <T extends Comparable<T>> Node<T> predecessor(Node<T> root) {
            while (root.getRightChild() != null) {
                root = root.getRightChild();
            }
            return root;
        }

        private static <T extends Comparable<T>> Node<T> successor(Node<T> root) {
            while (root.getLeftChild() != null) {
                root = root.getLeftChild();
            }
            return root;
        }

        public List<T> traverse() {
            return TraverseOrder.IN_ORDER.tread(root);
        }

        private static <T extends Comparable<T>> Node<T> insert(Node<T> node, T data) {
            if (node == null) {
                return new Node<>(data);
            }
            int result = data.compareTo(node.getData());
            if (result > 0) {
                node.setRightChild(insert(node.getRightChild(), data));
            } else if (result < 0) {
                node.setLeftChild(insert(node.getLeftChild(), data));
            }
            return node;
        }
    }
}
