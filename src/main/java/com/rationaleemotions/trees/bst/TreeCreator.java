package com.rationaleemotions.trees.bst;

import com.rationaleemotions.trees.Node;

import java.util.*;
import java.util.function.Function;

public class TreeCreator {

    public static void main(String[] args) {
        MyTree<Integer> tree = new MyTree<>();
        Function<String, Integer> mapper = text -> {
            try {
                return Integer.parseInt(text);
            } catch (NumberFormatException e) {
                return null;
            }
        };
        tree.createTree(mapper);
        System.err.println("Number of nodes = " + tree.count());
        System.err.println("Height = " + tree.height());
        for (TraverseOrder each : TraverseOrder.values()) {
            System.err.println("Recursively traversing in " + each.name() + " order gives ===> " + tree.traverse(each));
        }
        for (IterativeTraverseOrder each : IterativeTraverseOrder.values()) {
            System.err.println("Iteratively traversing in " + each.name() + " order gives ===> " + tree.traverse(each));
        }
    }

    public static class MyTree<T extends Comparable<T>> {
        private Node<T> root = null;

        public int height() {
            return height(root);
        }

        private static <T extends Comparable<T>> int height(Node<T> start) {
            if (start == null) {
                return 0;
            }
            return Math.max(height(start.getLeftChild()), height(start.getRightChild())) + 1;
        }

        public int count() {
            return count(root);
        }

        private static <T extends Comparable<T>> int count(Node<T> start) {
            if (start == null) {
                return 0;
            }
            return count(start.getLeftChild()) + count(start.getRightChild()) + 1;
        }

        public void createTree(Function<String, T> mapper) {
            Queue<Node<T>> queue = new ArrayDeque<>();
            System.err.println("Enter Root value :");
            Scanner reader = new Scanner(System.in);
            root = new Node<>(mapper.apply(reader.next()));
            queue.add(root);
            while (!queue.isEmpty()) {
                Node<T> ptr = queue.poll();
                System.err.println("Enter left child: ");
                T t = mapper.apply(reader.next());
                boolean leftAdded = false;
                if (t != null) {
                    ptr.setLeftChild(new Node<>(t));
                    queue.add(ptr.getLeftChild());
                    leftAdded = true;
                }
                boolean rightAdded = false;
                System.err.println("Enter right child: ");
                t = mapper.apply(reader.next());
                if (t != null) {
                    ptr.setRightChild(new Node<>(t));
                    queue.add(ptr.getRightChild());
                    rightAdded = true;
                }
                if (!leftAdded && !rightAdded) {
                    break;
                }
            }
        }

        public List<T> traverse(TraverseOrder order) {
            return order.tread(root);
        }

        public List<T> traverse(IterativeTraverseOrder order) {
            return order.tread(root);
        }
    }

}
