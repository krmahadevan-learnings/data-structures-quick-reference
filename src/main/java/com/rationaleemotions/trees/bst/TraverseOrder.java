package com.rationaleemotions.trees.bst;

import com.rationaleemotions.trees.Node;

import java.util.*;

public enum TraverseOrder implements Walk {
    IN_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            inOrder(start, contents);
            return contents;
        }

        private static <T extends Comparable<T>> void inOrder(Node<T> start, List<T> contents) {
            if (start == null) return;
            inOrder(start.getLeftChild(), contents);
            contents.add(start.getData());
            inOrder(start.getRightChild(), contents);
        }
    },
    PRE_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            preOrder(start, contents);
            return contents;
        }

        private static <T extends Comparable<T>> void preOrder(Node<T> start, List<T> contents) {
            if (start == null) return;
            contents.add(start.getData());
            preOrder(start.getLeftChild(), contents);
            preOrder(start.getRightChild(), contents);
        }

    },
    POST_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            postOrder(start, contents);
            return contents;
        }

        private static <T extends Comparable<T>> void postOrder(Node<T> start, List<T> contents) {
            if (start == null) return;
            postOrder(start.getLeftChild(), contents);
            postOrder(start.getRightChild(), contents);
            contents.add(start.getData());
        }
    },

    LEVEL_ORDER {
        @Override
        public <T extends Comparable<T>> List<T> tread(Node<T> start) {
            List<T> contents = new ArrayList<>();
            Queue<Node<T>> queue = new ArrayDeque<>();
            queue.add(start);
            while (!queue.isEmpty()) {
                Node<T> node = queue.poll();
                contents.add(node.getData());
                Optional.ofNullable(node.getLeftChild()).ifPresent(queue::add);
                Optional.ofNullable(node.getRightChild()).ifPresent(queue::add);
            }
            return contents;
        }
    }
}
