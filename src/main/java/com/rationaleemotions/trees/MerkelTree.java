package com.rationaleemotions.trees;

import lombok.Getter;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@Getter
//Reference: https://medium.com/@vinayprabhu19/merkel-tree-in-java-b45093c8c6bd
public class MerkelTree {

    public static void main(String[] args) {
        System.err.println(new MerkelTree(List.of(() -> "Krishnan Mahadevan")).getRootSha());
    }

    private final String rootSha;

    public MerkelTree(List<Stringify> input) {
        List<String> data = input.stream().map(Stringify::content).collect(Collectors.toList());
        rootSha = build(data, true).get(0);
    }

    private List<String> build(List<String> input, boolean isFirstInvocation) {
        if (input.size() == 1 && !isFirstInvocation) {
            //If the number of elements is one, then it means we have reached the root
            return input;
        }

        List<String> data = new ArrayList<>(input);
        if (input.size() % 2 != 0) {
            //We need the input size to always be even. If it's not so, then duplicate the last element
            //so that the input data set size becomes even.
            int elementToDuplicate = input.size() - 1;
            data.add(input.get(elementToDuplicate));
        }
        List<String> parentSha = new ArrayList<>();
        for (int i = 0; i < data.size(); i += 2) {
            String leftChildSha = data.get(i);
            String rightChildSha = data.get(i + 1);
            String calculatedSha = sha(leftChildSha + rightChildSha);
            parentSha.add(calculatedSha);
        }
        return build(parentSha, false);
    }
    private static String sha(String input) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        byte[] messageDigest = md.digest(input.getBytes(StandardCharsets.UTF_8));
        BigInteger noHash = new BigInteger(1, messageDigest);
        return noHash.toString(16);
    }
    public interface Stringify {
        String content();
    }
}
