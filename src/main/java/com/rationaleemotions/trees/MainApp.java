package com.rationaleemotions.trees;

import com.rationaleemotions.trees.avl.AVLTree;
import com.rationaleemotions.trees.redblack.RedBlackTree;

import java.util.Arrays;
import java.util.List;

public class MainApp {

    /*
     * Video Reference: https://youtu.be/bqOSo1f1jbo
     */
    public static void main(String[] args) {
        demoAvlTree();
        demoRedBlackTree();
    }

    private static void demoRedBlackTree() {
        List<Integer> data = Arrays.asList(10, 20, 5, 40, 50, 25, 60, 80, 85, 90, 30, 15, 75, 100, 55, 45, 0, 26);
        Tree<Integer> tree = new RedBlackTree<>();
        data.forEach(tree::insert);
        tree.traverse();

        System.out.println("Max is: " + tree.getMax());
        System.out.println("Min is: " + tree.getMin());
    }

    private static void demoAvlTree() {
        Tree<Integer> avlTree = new AVLTree<>();
        List<Integer> data = Arrays.asList(
                10, 2, 6, 8, 25, 18, 35, 15, 22, 42, 30, 40, 12, 17, 19, 24, 28, 33, 38
        );
        data.forEach(avlTree::insert);

        avlTree.traverse();

        System.out.println("Max is: " + avlTree.getMax());
        System.out.println("Min is: " + avlTree.getMin());

        System.out.println("Deleting 42 from Tree");
        avlTree.delete(42);

        System.out.println("New Max is: " + avlTree.getMax());
        avlTree.traverse();
    }

}
