package com.rationaleemotions.trees;

import lombok.*;

@Getter
@Setter
@ToString
public class Node<T extends Comparable<T>> {

    public enum Color {
        RED,
        BLACK
    }

    @NonNull
    private T data;

    private Color color = Color.RED;

    private Node<T> leftChild;
    private Node<T> rightChild;

    private int height = 1;

    @ToString.Exclude
    private Node<T> parent;

    public Node(T data) {
        this.data = data;
    }

    public boolean isLeftChildOfParent() {
        return this == parent.getLeftChild();
    }

    public boolean isRightChildOfParent() {
        return !isLeftChildOfParent();
    }

    public void flipColor() {
        setColor(color == Color.RED ? Color.BLACK : Color.RED);
    }

    public boolean isLeafNode() {
        return leftChild == null && rightChild == null;
    }

}
