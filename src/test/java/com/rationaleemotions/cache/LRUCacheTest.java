package com.rationaleemotions.cache;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LRUCacheTest {

    @Test
    public void happyFlow() {
        LRUCache cache = new LRUCache(3);
        cache.add("one", "one");
        cache.add("two", "two");
        cache.add("three", "three");
        System.err.println(cache.get("one"));
        System.err.println(cache.get("two"));
        cache.add("four", "four");
        Assert.assertNull(cache.get("three"));
    }

    @Test
    public void onlyAddingElements() {
        LRUCache cache = new LRUCache(3);
        cache.add("one", "one");
        cache.add("two", "two");
        cache.add("three", "three");
        cache.add("four", "four");
        Assert.assertNull(cache.get("one"));
    }
}
